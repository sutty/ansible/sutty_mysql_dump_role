Sutty MySQL Dump Role
=====================

Dumps all or some databases into a gzipped file.  Supports shared
hosting if `virtualenv` is present.

Example Playbook
----------------

```yaml
---
- hosts: "sutty"
  strategy: "free"
  remote_user: "root"
  tasks:
  - name: "role"
    include_role:
      name: "sutty_role"
    vars:
      mysql_user: "root"
      mysql_pass: "root"
      mysql_host: "localhost"
      mysql_databases:
      - "some"
```

License
-------

MIT-Antifa
